# Introduction

This project provides tutorials for the NtupleAnalysisUtils project (https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils).

Two kinds of tutorials are available: 
* A set of interactive jupyter notebooks, which explain the features in detail and allow for interactive experimentation
* A set of compiled programs that mirror or extend the notebook content, and which can be compiled with the standard ATLAS releases 

The interactive tutorial is split into several parts. It can be run either locally or in your browser, via CERN's SWAN service 

# Running the notebooks

## Running in SWAN

To run the tutorial in SWAN, please use the following link. When prompted, make sure to select an LCG release supplying the ROOT version matching the latest analysis releases - this is what is used to supply the compiled C++ files that are accessed by the tutorial. 

Currently, this is **LCG_97a**. 

[![text](http://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go?projurl=https://oauth2:z-gixzrerGWPVcHaDj9-@gitlab.cern.ch/goblirsc/NtupleAnalysisUtils_tutorial.git)

In SWAN, you can then on the different parts and work through them. 

## Running locally

To run locally, you can setup an LCG enviromnent (assuming you have cvmfs and eos on your machine): 
``` 
    lsetup "views LCG_97a  x86_64-centos7-gcc8-opt"
``` 
Then, you need to start a notebook server. This can be done via 
``` 
    root --notebook
``` 
the prompt above will open a web browser from which you can run the tutorials.

Alternatively: 
``` 
    root --notebook --no-browser
``` 
will run a command-line version of the server. You can then connect your favourite notebook viewer, for example VS Code (even in a remote session!) to this server using the URL that will be printed to the command line. 

# Running the compiled examples 

The easiest way is to clone the parent project, https://gitlab.cern.ch/goblirsc/NtupleAnalysisUtils, recursively.
It will provide this project as a submodule.

It can be built using the standard ATLAS CMake workflow, using an ATLAS (analysis or other) release. 

Then, the programs can be run directly in the command line.
Some will save pdf images with plots in the current working directory. 

Please feel free to change the programs and recompile to try out the various features! 
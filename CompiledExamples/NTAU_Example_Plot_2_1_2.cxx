#include "NtupleAnalysisUtils/NTAUTopLevelIncludes.h"

int main (int, char**){

    // Instantiate a new empty plot of user-configured binning
    Plot<TH1D> myHisto{ConstructInPlace<TH1D>("H1","H1",200,-2,2)}; /// "ConstructInPlace" will create a new, empty histogram according to the usual ROOT arguments. 

    // give it some dummy content
    myHisto->FillRandom("gaus",100000);  // Fill our histo a bit so that things become less boring
    

    SetAtlasStyle(); // no ATLAS root macro would be complete without! Of course, NtupleAnalysisUtils provides this method for you. 

    // For readability, we define the arguments separately before passing them into the Plot constructor.
    // If you want, you can do all this in one call ;-) 

    // First, we define the format we would like. Here, we go for a nice dark blue. 
    // We also prepare everything for drawing a legend (will not be done in this example)
    // Notice how we can daisy-chain arguments in arbitrary order to build our format! 
    PlotFormat myFormat = PlotFormat().MarkerStyle(kOpenCircle).Color(kBlue+1).FillStyle(1001).FillAlpha(0.3).LegendTitle("My cool Gauss").LegendOption("PL"); 

    /// Now, we put together a plot via Populator and Format. Here, we copy the histo we previously made
    Plot<TH1D> myPlot (CopyExisting<TH1D>(myHisto), myFormat); 

    /// please ignore for now - we just use this to save the plots in the 
    /// run dir, to keep things simple!
    CanvasOptions defaultOpts = CanvasOptions().OutputDir("."); 

    /// We can also make changes to the format later on! 
    myPlot.plotFormat().MarkerStyle(kFullDotLarge);  /// let's make the markers a bit nicer... 

    /// Opportunistic population means the object will be read from the input file in the following 
    /// line, triggered by our access to the content via the "->" operator! 
    /// Notice that the format will already be applied for us at the same time. 
    /// Note: We need to create a new canvas here - drawing with a fill style into an existiong one will
    /// crash the notebook... figure... 
    TCanvas* c1 = new TCanvas;
    c1->cd();
    c1->Draw(); 
    myPlot->Draw("HIST");
    c1->SaveAs( "ExamplePlot.pdf"); 

    myPlot.plotFormat().Color(kRed).MarkerStyle(kFullDiamond).FillStyle(0).FillColor(0);
    myPlot.applyFormat(); /// this triggers a re-application of the updated format

    c1->cd();
    myPlot->Draw("PL");
    c1->Draw(); 
    c1->SaveAs( "ExamplePlot.Step2.pdf"); 

    /// now let's copy the plot 
    Plot<TH1D> myOtherPlot = myPlot; /// copy via assignment operator. 
    myOtherPlot.plotFormat().MarkerStyle(kOpenSquare).Color(kBlue-6).FillStyle(0);  /// change the format of the copy a bit
    myOtherPlot.applyFormat(); 
    myOtherPlot->Scale(0.9); /// also scale it compared to the original

    /// Draw both - they will now be different, showing that both Plots contain distinct ROOT objects. 
    myPlot->Draw("PL");
    myOtherPlot->Draw("PLSAME");
    c1->Draw(); 
    c1->SaveAs( "ExamplePlot.Step3.pdf"); 


    return 0; 
}